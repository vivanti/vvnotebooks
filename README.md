vvnotebooks
===========

This Python module defines some useful abstractions for
writing Jupyter notebooks with a more rigorous approach
to the software engineering side.
